import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { FiBell, FiList, FiLogOut, FiUser } from "react-icons/fi";
import { Link } from 'react-router-dom';
import { UserAuth } from '../context/AuthContext';
import {
  NavbarToggler,
} from "reactstrap";

export default function TemporaryDrawer() {
  const { logout, getToken } = UserAuth();
  const [state, setState] = React.useState({
    left: false,
  });

  const handleLogout = async () => {
    try {
      await logout();
    } catch (e) {
      console.log(e.message);
    }
  };

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {['List', 'Notification', 'Profile', 'Logout'].map((text, index) => (
          <Link to={text === "List"  ? "/list" : text === "Notification" ? "/" : text === "Profile" ? "/profile" : ""}>
          <ListItem key={text} disablePadding onClick={text === "Logout" && handleLogout()}>
            <ListItemButton>
              <ListItemText primary={text} />
            </ListItemButton>
          </ListItem>
          </Link>
        ))}
      </List>
    </Box>
  );

  return (
    <div>
      {['left'].map((anchor) => (
        <React.Fragment key={anchor}>
          <NavbarToggler
            style={{ marginRight: "30px" }}
            onClick={toggleDrawer(anchor, true)}
            className="togglernav navtoggler"
          />
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
