import React, { useState, useEffect } from "react";
import { UserAuth } from "../context/AuthContext";
import { axiosInstance } from "../axios";
import NavbarMenu from "./Navbar";
import { useParams, useNavigate } from "react-router-dom";
import { Modal } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";

import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";

const NegotiationInfo = () => {
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm({
    criteriaMode: "all",
  });

  const handleClose = () => {
    setShow(false);
    navigate("/list");
  };
  const handleShow = () => {
    setShowConfirmation(false);
    try {
      updateNegotiation(idNegosiasi.idnego, 2);
    } catch (err) {
      console.log(err);
    }
    setShow(true);
  };

  const handleCloseConfirmation = () => setShowConfirmation(false);
  let handleShowConfirmation;

  const handleCloseRejection = () => setShowRejectedConfirmation(false);
  let handleRejectedConfirmation;

  const handleCloseProductStatus = () => setShowProductStatus(false);
  let handleStatus;

  const handleSoldProduct = () => {
    try {
      if (negotiationStatus === "approved") {
        UpdateProductStatus(product.id);
        window.alert("Status Produk Berhasil Diperbarui");
        navigate("/");
      } else {
        updateNegotiation(idNegosiasi.idnego, 3);
        window.alert("Status Produk Berhasil Diperbarui");
        navigate("/");
      }
    } catch (err) {
      console.error(err);
    }
  };

  const handleRejection = () => {
    try {
      updateNegotiation(idNegosiasi.idnego, 3);
      window.alert(`Penawaran ${idNegosiasi.name} Berhasil Ditolak`);
      navigate("/");
    } catch (err) {
      console.log(err);
    }
  };

  const { id } = useParams();
  const { updateNegotiation, UpdateProductStatus, getUserDatabase, getToken } =
    UserAuth();
  const [show, setShow] = useState(false);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [showRejectedConfirmation, setShowRejectedConfirmation] =
    useState(false);
  const [showProductStatus, setShowProductStatus] = useState(false);
  const [loaded, setLoaded] = useState(false);
  const [negotiator, setNegotiator] = useState([]);
  const [product, setProduct] = useState([]);
  const [idNegosiasi, setIdNegosiasi] = useState({});
  const [negotiationStatus, setNegotationStatus] = useState();

  useEffect(() => {
    if (!loaded) {
      const loadData = async () => {
        const negotiatedProduct = (await axiosInstance.get(`product/${id}`))
          .data;

        if (!negotiatedProduct) {
          navigate("/");
          window.alert("Penawaran Tidak Ditemukan");
        } else {
          const negotiationData = (
            await axiosInstance.get(`negotiation`, {
              headers: {
                productid: id,
              },
            })
          ).data;

          if (negotiationData.length > 0) {
            if (getUserDatabase().uid === negotiationData[0].seller_uid) {
              setProduct(negotiatedProduct);
              for (let i = 0; i < negotiationData.length; i++) {
                const negotiatorData = (
                  await axiosInstance.get(`profile`, {
                    headers: {
                      product: negotiationData[i].buyer_uid,
                      authorization: getToken(),
                    },
                  })
                ).data;
                negotiatorData.price = negotiationData[i].price;
                negotiatorData.idnego = negotiationData[i].id;
                negotiatorData.isApproved = negotiationData[i].isApproved;
                setNegotiator((current) => [negotiatorData, ...current]);
              }
            } else {
              navigate("/");
              window.alert("You Are Not Supposed To Be Here");
            }
          } else {
            navigate("/");
            window.alert("You Are Not Supposed To Be Here");
          }
        }
      };
      loadData().then(() => {
        setLoaded(true);
      });
    }
  }, [loaded]);

  return (
    <>
      <NavbarMenu />
      {!loaded ? (
        <Backdrop
          sx={{
            backgroundColor: "#FFFFFF",
            color: "#7126b5",
            zIndex: (theme) => theme.zIndex.drawer + 1,
          }}
          open={!loaded}
        >
          <Box display={"flex"} flexDirection={"column"}>
            <CircularProgress color="inherit" sx={{ margin: "auto", mb: 1 }} />
            <Typography variant="h5">Loading</Typography>
          </Box>
        </Backdrop>
      ) : (
        <>
          <div className="row d-flex justify-content-center">
            <h2 className="text-center">DETAIL PRODUCT</h2>
            <div
              className="col-lg-8"
              style={{ border: "2px solid black", borderRadius: "10px" }}
            >
              <div className="detailProduct">
                <img
                  src={
                    product.images && product.images.length > 0
                      ? product.images[0]
                      : "/img/jam1.png"
                  }
                  alt="Foto Barang Yang Ditawar"
                  width="100px"
                />
                <p>{product.name}</p>
                <p>Rp {product.price}</p>
                {!product.isAvailable && (
                  <div className="button-container d-flex justify-content-end">
                    <strong>
                      <h3>Barang Sudah Terjual</h3>
                    </strong>
                  </div>
                )}
              </div>
            </div>
            <h2 className="text-center">DAFTAR PENAWAR</h2>
            {Object.entries(negotiator).map(([key, item]) => (
              <div
                className="col-lg-8 mt-3 p-3"
                style={{ border: "2px solid black", borderRadius: "10px" }}
                key={key}
              >
                <img src={item.image} alt="Foto Penawar" width="200px" />
                <p>Nama Penawar: {item.name}</p>
                <p>Kota: {item.city}</p>
                <p>Ditawar: Rp {item.price}</p>
                {product.isAvailable && (
                  <div>
                    {item.isApproved === 2 ? (
                      <div className="button-container d-flex justify-content-end">
                        <button
                          className="btn-sign-google text-center"
                          onClick={
                            (handleStatus = () => {
                              setIdNegosiasi(item);
                              setShowProductStatus(true);
                            })
                          }
                        >
                          Status
                        </button>
                        &nbsp;
                        <a
                          href={`https://wa.me/${item.phone}`}
                          className="btn-sign tex-center nextButton"
                          target="_blank"
                        >
                          Hubungi di WhatsApp
                        </a>
                      </div>
                    ) : (
                      <div className="button-container d-flex justify-content-end">
                        {item.isApproved === 3 ? (
                          <p>
                            <strong>Ditolak</strong>
                          </p>
                        ) : (
                          <div>
                            <button
                              className="btn-sign-google text-center"
                              onClick={
                                (handleRejectedConfirmation = () => {
                                  setIdNegosiasi(item);
                                  setShowRejectedConfirmation(true);
                                })
                              }
                            >
                              Tolak
                            </button>
                            &nbsp;
                            <button
                              className="btn-sign tex-center nextButton"
                              onClick={
                                (handleShowConfirmation = () => {
                                  setIdNegosiasi(item);
                                  setShowConfirmation(true);
                                })
                              }
                            >
                              Terima
                            </button>
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                )}
              </div>
            ))}
          </div>
          <Modal show={showConfirmation} centered>
            <Modal.Body>
              <div className="card-body">
                <h6 className="card-title ">PERINGATAN</h6>
                <p className="text-muted">
                  Apakah Anda Yakin Ingin Menerima Penawaran Ini?
                </p>
                <div className="button-container d-flex justify-content-end">
                  <button
                    onClick={handleCloseConfirmation}
                    className="btn-sign-google text-center"
                  >
                    Batal
                  </button>
                  &nbsp;
                  <button
                    className="btn-sign tex-center nextButton"
                    onClick={handleShow}
                  >
                    Terima
                  </button>
                </div>
              </div>
            </Modal.Body>
          </Modal>
          <Modal show={showRejectedConfirmation} onHide={handleClose} centered>
            <Modal.Body>
              <div className="card-body">
                <h6 className="card-title ">PERINGATAN</h6>
                <p className="text-muted">
                  Apakah Anda Yakin Ingin Menolak Penawaran Ini?
                </p>
                <div className="button-container d-flex justify-content-end">
                  <button
                    onClick={handleCloseRejection}
                    className="btn-sign-google text-center"
                  >
                    Batal
                  </button>
                  &nbsp;
                  <button
                    className="btn-sign tex-center nextButton"
                    onClick={handleRejection}
                  >
                    Tolak
                  </button>
                </div>
              </div>
            </Modal.Body>
          </Modal>
          <Modal
            show={showProductStatus}
            onHide={handleCloseProductStatus}
            centered
          >
            <Modal.Header closeButton className="border-0"></Modal.Header>
            <Modal.Body>
              <div className="card-body">
                <h6 className="card-title ">
                  Perbarui Status Penjualan Produkmu
                </h6>
                <div className="button-container d-flex justify-content-end">
                  <div className="form-check">
                    <input
                      {...register("statusNego", {
                        required: "Tolong Pilih Status",
                      })}
                      className="form-check-input"
                      type="radio"
                      value="approved"
                      name="flexRadioDefault"
                      onChange={(e) => setNegotationStatus(e.target.value)}
                      id="flexRadioDefault1"
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault1"
                    >
                      Berhasil Terjual
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      {...register("statusNego", {
                        required: "Tolong Pilih Status",
                      })}
                      className="form-check-input"
                      type="radio"
                      value="rejected"
                      name="flexRadioDefault"
                      onChange={(e) => setNegotationStatus(e.target.value)}
                      id="flexRadioDefault2"
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexRadioDefault2"
                    >
                      Batalkan Transaksi
                    </label>
                  </div>
                  <ErrorMessage
                    errors={errors}
                    name="statusNego"
                    render={({ messages }) =>
                      messages &&
                      Object.entries(messages).map(([type, message]) => (
                        <p key={type}>{message}</p>
                      ))
                    }
                  />
                  <button
                    className="btn-sign tex-center nextButton"
                    onClick={handleSubmit(handleSoldProduct)}
                    disabled={isSubmitting}
                  >
                    Kirim
                  </button>
                </div>
              </div>
            </Modal.Body>
          </Modal>
          <Modal show={show} onHide={handleClose} centered>
            <Modal.Header closeButton className="border-0"></Modal.Header>
            <Modal.Body>
              <div className="card-body">
                <h6 className="card-title ">
                  Yeay kamu berhasil mendapatkan harga yang sesuai
                </h6>
                <p className="text-muted">
                  Segera hubungi pembeli melalui whatsapp untuk transaksi
                  selanjutnya
                </p>
                <div className="card mb-3 inputBasic">
                  <h5 className="text-center">Product Match</h5>
                  <hr />
                  <img
                    src={idNegosiasi.image}
                    alt="Foto Penawar"
                    width="80px"
                  />
                  <p>{idNegosiasi.name}</p>
                  <p>{idNegosiasi.city}</p>
                  <hr />
                  <img
                    src={
                      product.images && product.images.length > 0
                        ? product.images[0]
                        : "/img/jam1.png"
                    }
                    alt="Foto Barang Yang Ditawar"
                    width="75px"
                  />
                  <p>{product.name}</p>
                  <p>
                    <s>Rp {product.price}</s>
                  </p>
                  <p>Ditawar Rp {idNegosiasi.price}</p>
                  <a
                    href={`https://wa.me/${idNegosiasi.phone}`}
                    className="btn-sign tex-center nextButton"
                    target="_blank"
                  >
                    Hubungi via WhatsApp
                  </a>
                </div>
              </div>
            </Modal.Body>
          </Modal>
        </>
      )}
    </>
  );
};

export default NegotiationInfo;
