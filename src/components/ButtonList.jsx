import React from "react";
import {
  FiBox,
  FiChevronRight,
  FiDollarSign,
  FiHeart,
  FiSearch,
} from "react-icons/fi";
import {
  CardBody,
  CardGroup,
  CardSubtitle,
  CardText,
  CardTitle,
} from "reactstrap";

const ButtonList = ({ setList }) => {
  return (
    <div className="col-md-3 .offset-md-1">
      <div className="cardd-1">
        <CardGroup>
          <CardBody>
            <CardTitle className="fw-bold">Kategori</CardTitle>
            <div className="">
              <button type="button" className="btn d-flex">
                <div
                  className="d-flex"
                  style={{ justifyContent: "space-between" }}
                ></div>
                <div className="d-flex" onClick={() => setList("produk")}>
                  <h6>
                    <FiBox className="me-1" style={{ fontSize: "24px" }} />
                    Semua Produk
                  </h6>
                  <FiChevronRight
                    style={{ marginLeft: "10px", fontSize: "24px" }}
                  />
                </div>
              </button>
              <hr />
              <button
                type="button"
                className="btn d-flex"
                style={{ justifyContent: "space-between" }}
              >
                <div className="d-flex" onClick={() => setList("diminati")}>
                  <h6>
                    <FiHeart className="me-1" style={{ fontSize: "24px" }} />
                    Diminati
                  </h6>
                  <FiChevronRight
                    style={{ marginLeft: "60px", fontSize: "24px" }}
                  />
                </div>
              </button>
              <hr />
              <button type="button" className="btn d-flex">
                <div
                  className="d-flex"
                  style={{ justifyContent: "space-between" }}
                >
                  <div className="d-flex" onClick={() => setList("terjual")}>
                    <h6>
                      <FiDollarSign
                        className="me-1"
                        style={{ fontSize: "24px" }}
                      />
                      Terjual
                    </h6>
                    <FiChevronRight
                      style={{ marginLeft: "71px", fontSize: "24px" }}
                    />
                  </div>
                </div>
              </button>
            </div>
          </CardBody>
        </CardGroup>
      </div>
    </div>
  );
};

export default ButtonList;
