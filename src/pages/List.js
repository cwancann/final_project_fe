import React, { useState } from "react";
import ButtonList from "../components/ButtonList";
import ListProduct from "../components/ListProduct";
import Negotiation from "../components/Negotiation";
import Purchased from "../components/Purchased";
import SemuaProduct from "../components/SemuaProduct";

const List = () => {
  const [list, setList] = useState("produk");
  // console.log(list === "product");
  return (
    <div>
      <ListProduct />
      <div className="container mt-4">
        <div className="row">
          <ButtonList setList={setList} clicked={list} />
          {list === "produk" ? (
            <SemuaProduct />
          ) : list === "diminati" ? (
            <Negotiation />
          ) : list === "terjual" ? (
            <Purchased />
          ) : (
            <SemuaProduct />
          )}
        </div>
      </div>
    </div>
  );
};

export default List;
