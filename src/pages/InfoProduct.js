import React, { useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import NavbarMenu from "../components/Navbar";
import { Link } from "react-router-dom";
import { FaWhatsapp } from "react-icons/fa";

export default function InfoModal() {
  const [show1, setShow1] = useState(false);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);

  const [show2, setShow2] = useState(false);
  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);

  return (
    <div className="InfoProduct">
      <NavbarMenu />
      <div className="row justify-content-center g-0 form">
        <div className="col-md-1 col-xs-1">
          <Link to="/">
            <img src="img/left.png" className="me-2 p-4" alt="" />
          </Link>
        </div>
        <div div className="col-md-4 col-xs-12">
          <div className="card mb-3 inputBasic">
            <div class="row no-gutters align-items-center">
              <div className="col-3">
                <div className="card-body">
                  <img
                    src="https://images.unsplash.com/photo-1573496359142-b8d87734a5a2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=388&q=80"
                    className="card-img border-0 inputBasic img-user"
                    alt=""
                  ></img>
                </div>
              </div>
              <div className="col-8 ">
                <div className="card-body">
                  <p className="card-text m-0">Nama Penjual</p>
                  <p className="text-muted m-0">
                    <small>Kota</small>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div>
            <h6>Daftar Produkmu yang Ditawar</h6>
            <div className="card mb-3 inputBasic border-0">
              <div class="row align-items-start">
                <div className="col-3">
                  <img
                    src="https://images.unsplash.com/photo-1577993944451-f8618a835822?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=406&q=80"
                    className="img-produk3 border-0 inputBasic"
                    alt=""
                  ></img>
                </div>
                <div className="col-9">
                  <div className="card-body p-0">
                    <div className="row">
                      <div className="col auto small">
                        <p className="text-muted  m-0">
                          <small>Penawaran produk</small>
                        </p>
                      </div>
                      <div className="col auto text-end small">
                        <p className="text-muted  m-0">
                          <small>20 Apr, 14:04</small>
                        </p>
                      </div>
                    </div>
                    <p className="h6 m-0">Jam Tangan Casio</p>
                    <p className="h6">Rp 250.000</p>
                    <p className="h6">Di tawar Rp 200.000</p>
                  </div>
                </div>
              </div>
            </div>,
          </div>
          <div className="row">
            <div className="col-4"></div>
            <div className="col-8">
              <div
                className="d-flex mb-2"
                style={{ width: "100%", gap: "1rem" }}
              >
                <Button
                  type="submit"
                  className="btn-sign-google text-center"
                  style={{ width: "50%", borderColor: "#7126b5" }}
                  size="sm"
                >
                  Tolak
                </Button>
                <Button
                  type="submit"
                  className="btn-sign text-center"
                  style={{ width: "50%" }}
                  onClick={handleShow2}
                  size="sm"
                >
                  Terima
                </Button>
                <Modal show={show2} onHide={handleClose2} centered>
                  <Modal.Header closeButton className="border-0"></Modal.Header>
                  <Modal.Body>
                    <div className="card-body">
                      <h5 className="card-title">
                        Yeay kamu berhasil mendapat harga yang sesuai
                      </h5>
                      <p className="text-muted">
                        Segera hubungi pembeli melalui whatsapp untuk transaksi
                        selanjutnya
                      </p>
                      <div className="card mb-3 inputBasic bg-light border-0">
                        <div class="row no-gutters align-items-center ">
                          <h5
                            className="card-title text-center"
                            style={{ marginTop: "0.5rem" }}
                          >
                            Product Match
                          </h5>
                          <div className="row">
                            <div className="col-4">
                              <div className="card-body">
                                <img
                                  src="https://images.unsplash.com/photo-1573496359142-b8d87734a5a2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=388&q=80"
                                  className="card-img border-0 inputBasic img-produk2 "
                                  alt=""
                                ></img>
                              </div>
                            </div>
                            <div className="col-8">
                              <div className="card-body">
                                <p className="card-text p-0 m-0">
                                  Nama Pembeli
                                </p>
                                <p className="card-text p-0 m-0 text-muted">
                                  Kota
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-4">
                              <div className="card-body">
                                <img
                                  src="https://images.unsplash.com/photo-1577993944451-f8618a835822?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=406&q=80"
                                  className="card-img border-0 inputBasic img-produk2 "
                                  alt=""
                                ></img>
                              </div>
                            </div>
                            <div className="col-8">
                              <div className="card-body">
                                <p className="card-text p-0 m-0">
                                  Jam Tangan Casio
                                </p>
                                <p className="card-text p-0 m-0">
                                  <del>Rp 250.000</del>
                                </p>
                                <p className="card-text p-0 m-0">
                                  Ditawar Rp 200.000
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <Button
                        type="submit"
                        className="btn-sign text-center w-100"
                        onClick={handleClose2}
                      >
                        Hubungi via Whatsapp
                        <FaWhatsapp style={{ marginLeft: "2rem" }} />
                      </Button>
                    </div>
                  </Modal.Body>
                </Modal>
              </div>
              <div
                className="d-flex mb-2"
                style={{ width: "100%", gap: "1rem" }}
              >
                <Button
                  type="submit"
                  className="btn-sign-google text-center"
                  style={{ width: "50%", borderColor: "#7126b5" }}
                  onClick={handleShow1}
                  size="sm"
                >
                  Status
                </Button>
                <Button
                  type="submit"
                  className="btn-sign text-center"
                  style={{ width: "50%" }}
                  size="sm"
                >
                  Hubungi di
                  <FaWhatsapp style={{ marginLeft: "0.5rem" }} />
                </Button>
                <Modal show={show1} onHide={handleClose1} centered>
                  <Modal.Header closeButton className="border-0"></Modal.Header>
                  <Modal.Body>
                    {/* <div className="card-body">
                      <h5 className="card-title">
                        Perbarui status penjualan produkmu
                      </h5>
                      <Form style={{ paddingTop: "1rem" }}>
                        {["radio"].map((type) => (
                          <div key={`reverse-${type}`} className="mb-3">
                            <div className="row">
                              <div className="col-1">
                                <Form.Check
                                  reverse
                                  name="group1"
                                  type={type}
                                  id={`reverse-${type}-1`}
                                  inline
                                />
                              </div>
                              <div className="col-11">
                                <p className="h6">Berhasil terjual</p>
                                <p className="text-muted ">
                                  Kamu telah sepakat menjual produk ini kepada
                                  pembeli
                                </p>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-1">
                                <Form.Check
                                  reverse
                                  name="group1"
                                  type={type}
                                  id={`reverse-${type}-1`}
                                  inline
                                />
                              </div>
                              <div className="col-11">
                                <p className="h6">Batalkan transaksi</p>
                                <p className="text-muted ">
                                  Kamu membatalkan transaksi produk ini dengan
                                  pembeli
                                </p>
                              </div>
                            </div>
                          </div>
                        ))}
                      </Form>
                      <Button
                        type="submit"
                        className="btn-sign text-center w-100"
                        onClick={handleClose1}
                      >
                        Kirim
                      </Button>
                    </div> */}
                    <div className="card-body">
                      <h5 className="card-title">
                        Perbarui status penjualan produkmu
                      </h5>
                      <Form style={{ paddingTop: "1rem" }}>
                        {["radio"].map((type) => (
                          <div key={`reverse-${type}`} className="mb-3">
                            <div className="row">
                              <div className="col-1">
                                <Form.Check
                                  reverse
                                  disabled
                                  name="group1"
                                  type={type}
                                  id={`reverse-${type}-1`}
                                  inline
                                />
                              </div>
                              <div className="col-11">
                                <p className="h6">Berhasil terjual</p>
                                <p className="text-muted ">
                                  Kamu telah sepakat menjual produk ini kepada
                                  pembeli
                                </p>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-1">
                                <Form.Check
                                  disabled
                                  reverse
                                  name="group1"
                                  type={type}
                                  id={`reverse-${type}-1`}
                                  inline
                                />
                              </div>
                              <div className="col-11">
                                <p className="h6">Batalkan transaksi</p>
                                <p className="text-muted ">
                                  Kamu membatalkan transaksi produk ini dengan
                                  pembeli
                                </p>
                              </div>
                            </div>
                          </div>
                        ))}
                      </Form>
                      <Button
                        disabled
                        type="submit"
                        className="btn-sign text-center w-100"
                      >
                        Kirim
                      </Button>
                    </div>
                  </Modal.Body>
                </Modal>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-1 col-xs-1 me-2"></div>
      </div>
    </div>
  );
}
